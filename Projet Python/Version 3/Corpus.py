import re
import pandas as pd
import numpy as np
from datetime import datetime
from scipy.sparse import csr_matrix
from sklearn.metrics.pairwise import cosine_similarity
from collections import Counter
from Author import Author
from Document import Document, RedditDocument, ArxivDocument

class Corpus:
    _instance = None
    
    def __new__(cls, *args, **kwargs):
        if not isinstance(cls._instance, cls):
            cls._instance = super().__new__(cls)
        return cls._instance
    
    def __init__(self, name):
        self.name = name # Nom du Corpus
        self.authors = {}  # Dictionnaire pour stocker les auteurs
        self.id2doc = {}  # Dictionnaire pour stocker les documents
        self.ndoc = 0  # Compteur pour les documents
        self.naut = 0  # Compteur pour les auteurs
        self.passages_concatenated = None # Passages concaténés de tous les documents

    def ajouter_document(self, document: Document):
        """
        Ajout du document au dictionnaire id2doc
        """ 
        self.id2doc[self.ndoc] = document
        self.ndoc += 1

        # Ajout de l'auteur au dictionnaire des auteurs ou mise à jour de la production documentaire de l'auteur
        if document.author not in self.authors:
            auteur = Author(document.author)
            self.authors[document.author] = auteur
            self.naut += 1
        self.authors[document.author].ajouter_document(document)

    def afficher_par_date(self, num_documents):
        # Trier les documents par date en utilisant la fonction "sorted".
        documents_tries = sorted(self.id2doc.values(), key=lambda x: x.date)[:num_documents]
        print(f"Documents triés par date (top {num_documents}):")
        for document in documents_tries:
            print(f"Date : {document.date}, Titre : {document.title}")

    def afficher_par_titre(self, num_documents):
        # Trier les documents par titre alphabétiquement en utilisant la fonction "sorted".
        documents_tries = sorted(self.id2doc.values(), key=lambda x: x.title)[:num_documents]
        print(f"Documents triés par titre (top {num_documents}):")
        for document in documents_tries:
            print(f"Titre : {document.title}, Date : {document.date}")


    def creer_passage(self):
        """
        Si passages_concatenated est None
        """
        if not self.passages_concatenated:
            # Initialiser avec une chaîne vide
            self.passages_concatenated = ""
            # Parcourir tous les documents dans le dictionnaire id2doc et concaténer le champ texte.
            for document in self.id2doc.values():
                self.passages_concatenated += str(document.text) + " "
                    
        return self.passages_concatenated


    def rechercher(self, mot_cle):
        """
        Recherche le mot clé donné dans les passages, le passage combiné est divisé par des points pour créer des passages individuels.
        """
        self.creer_passage()

        # Diviser le texte en passages basés sur les points
        passages = re.split(r'(?<=[.])\s+', self.passages_concatenated)

        # Trouver les passages contenant le mot clé en utilisant des expressions régulières
        correspondances = []
        for passage in passages:
            if re.search(r'\b' + re.escape(mot_cle) + r'\b', passage, flags=re.IGNORECASE):
                correspondances.append(passage)
        # Retourner la liste de correspondances
        return correspondances

    def concordance(self, mot_cle):
        """
        Fonction qui renvoie un DataFrame Pandas montrant la concordance du mot clé.
        """
        self.creer_passage()

        # Diviser le texte en passages basés sur les points
        passages = re.split(r'(?<=[.])\s+', self.passages_concatenated)

        # Trouver les passages contenant le mot clé en utilisant des expressions régulières
        # Créer un DataFrame Pandas pour stocker les données
        data = {
            'Contexte gauche': [],
            'Motif trouvé': [],
            'Contexte droit': []
        }

        for passage in passages:
            # Trouver la concordance avec regex pour les 15 premiers et derniers caractères en se limitant au point le plus proche.
            correspondances = re.finditer(r'(.{0,15})(' + re.escape(mot_cle) + r')(.{0,15})', passage, flags=re.IGNORECASE)
            for correspondance in correspondances:
                contexte_gauche = correspondance.group(1)
                motif_trouve = correspondance.group(2)
                contexte_droit = correspondance.group(3)
                
                data['Contexte gauche'].append(contexte_gauche)
                data['Motif trouvé'].append(motif_trouve)
                data['Contexte droit'].append(contexte_droit)

        # Retourner le DataFrame Pandas
        return pd.DataFrame(data)

    def nettoyer_texte(self, chaine_caracteres):
        """
        Nettoie une chaîne donnée en la convertissant en minuscules et en remplaçant "\n" par " ".
        Il supprime également la ponctuation et les chiffres.
        """
        # Convertir le texte en minuscules
        texte_nettoye = chaine_caracteres.lower()
        # Remplacer les sauts de ligne '\n'
        texte_nettoye = texte_nettoye.replace('\n', ' ')
        # Supprimer la ponctuation en utilisant des expressions régulières
        texte_nettoye = re.sub(r'[^\w\s]', '', texte_nettoye)
        # Supprimer les chiffres en utilisant des expressions régulières
        return re.sub(r'\d+', '', texte_nettoye)

    def statistiques(self, n=10):
        """
        Afficher différentes statistiques.
        1. Nombre de mots différents dans le corpus.
        2. N mots les plus fréquents.
        3. Fréquence des mots dans le corpus.
        4. DataFrame Pandas des mots, de leur fréquence et du nombre de documents dans lesquels ces mots se trouvent.
        """
        print("\n--------------- STATISTIQUES ---------------\n")
        self.creer_passage()

        texte_nettoye = self.nettoyer_texte(self.passages_concatenated)
        # ========== Nombre de mots différents =========
        
        # Diviser le paragraphe en mots
        mots = re.findall(r'\b\w+\b', texte_nettoye)

        # Compter le nombre de mots différents
        mots_differents = len(set(mots))

        # Afficher le nombre de mots différents
        print(f"Le nombre de mots différents dans le paragraphe est : {mots_differents}\n")
        
        # ========== N mots les plus fréquents ==============
        
        # Compter les occurrences de chaque mot
        compteur_mots = Counter(mots)

        # Obtenir les n mots les plus courants
        mots_plus_communs = compteur_mots.most_common(n)
        for mot in mots_plus_communs:
            print(f"La fréquence du mot '{mot[0]}' est {mot[1]}")

        # ========== Construction du vocabulaire =============
        vocabulaire = set()  # Utiliser un ensemble pour éliminer les doublons
        # Diviser le texte nettoyé en utilisant divers délimiteurs
        mots = re.split(r'[ \t.,;!?]+', texte_nettoye)
        vocabulaire.update(mots)
        if "" in vocabulaire:
            vocabulaire.remove("")

        # Convertir l'ensemble de vocabulaire en un dictionnaire pour l'indexation (si nécessaire)
        dictionnaire_vocabulaire = {index: mot  for index, mot in enumerate(vocabulaire)}
        print("\nDictionnaire de vocabulaire")
        print(dictionnaire_vocabulaire)

        # ========== Nombre d'occurrences de chaque mot dans le vocabulaire ===========
        mots_dans_paragraphe = re.findall(r'\b\w+\b', texte_nettoye)
        # Compter les occurrences de mots du vocabulaire dans le paragraphe
        occurrences_mots = {mot: 0 for mot in dictionnaire_vocabulaire.values()}
        for mot in mots_dans_paragraphe:
            if mot in vocabulaire:
                occurrences_mots[mot] += 1

        # Convertir les occurrences de mots en un DataFrame Pandas (tableau de fréquences)
        tableau_frequences = pd.DataFrame(list(occurrences_mots.items()), columns=['Mot', 'Fréquence'])
        
        # =========== Ajouter le nombre de documents au tableau de fréquences =============
        nb_doc = []
        for mot in tableau_frequences['Mot']:
            compteur = 0
            for doc in self.id2doc.values():
                if mot in self.nettoyer_texte(doc.text):
                    compteur += 1
            nb_doc.append(compteur)
        
        tableau_frequences['Fréquence Documentaire'] = nb_doc

        print("\nTableau de Fréquences")
        print(tableau_frequences)

    
    def creer_vocabulaire(self):
        """
        Nettoie le passage combiné et obtient une liste de mots, les trie par ordre alphabétique et leur ajoute des métadonnées.
        """
        self.creer_passage()
        
        # Nettoyer le texte
        texte_nettoye = self.nettoyer_texte(self.passages_concatenated)

        # Obtenir les mots du texte nettoyé.
        mots = re.findall(r'\b\w+\b', texte_nettoye)

        # Créer un dictionnaire de vocabulaire et parcourir les mots uniques triés par ordre alphabétique.
        vocabulaire = {}
        identifiant_mot = 0
        for mot in sorted(set(mots)):
            info_mot = {
                'id': identifiant_mot,
                'occurrences_totales': mots.count(mot),
                # Ajouter plus d'informations si nécessaire
            }
            vocabulaire[mot] = info_mot
            identifiant_mot += 1
        
        return vocabulaire


    def creer_matrice_tf(self):
        """
        Convertit le dictionnaire de vocabulaire en csr_matrix et ajoute ensuite la fréquence documentaire.
        """
        vocabulaire = self.creer_vocabulaire()

        nb_documents = len(self.id2doc)
        nb_mots = len(vocabulaire)
        
        # [Nb de docs x Nb de mots], initialisé à 0
        matrice_tf = [[0] * nb_mots for _ in range(nb_documents)]

        # Parcourir chaque document.
        for id_doc, document in self.id2doc.items():
            # Nettoyer le texte et obtenir les mots du document.
            texte_nettoye = self.nettoyer_texte(document.text)
            mots_doc = re.findall(r'\b\w+\b', texte_nettoye)
            
            for mot in mots_doc:
                info_mot = vocabulaire.get(mot)
                if info_mot is not None:
                    matrice_tf[id_doc][info_mot["id"]] += 1  # Soustraire 1 de id_doc pour correspondre à l'indexation de la matrice

        # Calculer le nombre total d'occurrences dans le corpus et le nombre total de
        # documents contenant ce mot. Stocker les informations dans vocabulaire
        self.mat_TF = csr_matrix(matrice_tf, shape=(nb_documents, nb_mots))
        
        mots = vocabulaire.keys()
        identifiant_mot = 0

        for mot in mots:
            occ_totale = 0
            for i in range(self.mat_TF.shape[0]):
                occ_totale += self.mat_TF[i, identifiant_mot]

            dans_document = 0
            for doc in self.id2doc.values():
                if mot in self.nettoyer_texte(doc.text):
                    dans_document += 1

            vocabulaire[mot] = {
                'id': identifiant_mot,
                'occurrences_totales': occ_totale,
                'dans_document': dans_document
            }

            identifiant_mot += 1

        self.vocabulaire = vocabulaire
        return vocabulaire


    def obtenir_vecteur_requete(self, requete):
        """
        Crée un vecteur à partir de la requête de l'utilisateur.
        """
        vocabulaire = self.creer_vocabulaire()
        texte_nettoye = self.nettoyer_texte(requete)
        mots_requete = re.findall(r'\b\w+\b', texte_nettoye)
        
        # [Nb de docs x Nb de mots], initialisé à 0
        matrice_tf = [[0] * len(vocabulaire)]
        
        for mot in mots_requete:
            info_mot = vocabulaire.get(mot)
            if info_mot is not None:
                matrice_tf[0][info_mot["id"]] += 1
            
        # Calculer le nombre total d'occurrences dans le corpus et le nombre total de
        # documents contenant ce mot. Stocker les informations dans vocabulaire
        return csr_matrix(matrice_tf, shape=(1 , len(vocabulaire)))


    def moteur_recherche(self, requete):
        """
        Calculer la similarité entre le vecteur du document et le vecteur de la requête.
        Trier les résultats et afficher la valeur de pourcentage de correspondance.
        """
        if self.mat_TF is None:
            self.creer_matrice_tf()
        
        similarite_doc = cosine_similarity(self.mat_TF, self.obtenir_vecteur_requete(requete))
        
        # Trier les valeurs de similarité par ordre décroissant et obtenir les indices correspondants
        indices_tries = np.argsort(-similarite_doc, axis=0)
        
        # Afficher les phrases pour chaque valeur de similarité et son index
        for _, idx in enumerate(indices_tries):
            index = idx[0]
            pourcentage_corr = similarite_doc[index][0] * 100
            print(f"La requête correspond à {pourcentage_corr:.2f} pour cent avec le document {index + 1}")



    def __repr__(self):
        return f"Corpus('{self.name}')"
    
    def sauvegarder(self):
        """
        Enregistrer les documents dans un fichier CSV.
        """
        donnees = {
            'ID du Document': [],
            'Titre': [],
            'Auteur': [],
            'Date': [],
            'URL': [],
            'Texte': [],
            'Type': [],
            'Commentaires': [],
            "Auteurs": []
        }

        for id_doc, document in self.id2doc.items():
            donnees['ID du Document'].append(id_doc)
            donnees['Titre'].append(document.title)
            donnees['Auteur'].append(document.author)
            donnees['Date'].append(document.date)
            donnees['URL'].append(document.url)
            donnees['Texte'].append(document.text)
            donnees['Type'].append(document.type)
            if document.type == "Reddit":
                donnees['Commentaires'].append(document.obtenir_num_comments())
                donnees["Auteurs"].append(None)
            elif document.type == "Arxiv":
                donnees['Commentaires'].append(None)
                donnees["Auteurs"].append(document.obtenir_authors())
            else:
                donnees['Commentaires'].append(None)
                donnees["Auteurs"].append(None)

        dataframe = pd.DataFrame(donnees)
        dataframe.to_csv("corpus.csv", index=False)
        print(f"Corpus enregistré sous 'corpus.csv'")

    @classmethod
    def charger(cls, nom_fichier):
        """Charger les documents à partir du fichier CSV."""
        df = pd.read_csv(nom_fichier)

        corpus = cls(name="Corpus Chargé")
        for index, ligne in df.iterrows():
            
            fabrique = DocumentGenerator()
            
            # Pour gérer l'absence de texte.
            if not isinstance(ligne['Texte'], str):
                ligne['Texte'] = ""

            document = fabrique.creer_document(ligne['Titre'], ligne['Auteur'], ligne['Date'], ligne['URL'], ligne['Texte'], ligne["Type"], num_comments=ligne["Commentaires"], authors=ligne["Auteurs"])
            corpus.ajouter_document(document)

        print(f"Corpus chargé depuis '{nom_fichier}'")
        return corpus

class DocumentGenerator:
    @staticmethod
    def creer_document(title, author, date, url, text, type, num_comments = None, authors = None):
        if type == "Reddit":
            return RedditDocument(title, author, date, url, text, type, num_comments)
        elif type == "Arxiv":
            return ArxivDocument(title, author, date, url, text, type, authors)
        else:
            return Document(title, author, date, url, text, type)