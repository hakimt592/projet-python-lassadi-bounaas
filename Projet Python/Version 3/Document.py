class Document:
    def __init__(self, title, author, date, url, text, type):
        self.title = title
        self.author = author
        self.date = date
        self.url = url
        self.text = text
        self.type = type

    def afficher_info(self):
        print(f"Titre : {self.title}")
        print(f"Auteur : {self.author}")
        print(f"Date : {self.date}")
        print(f"URL : {self.url}")
        print(f"Texte : {self.text}")
        print(f"Type : {self.type}")

    def obtenirType(self):
        return self.type

    def __str__(self):
        return f"Titre : {self.title}"

class RedditDocument(Document):
    def __init__(self, title, author, date, url, text, type, num_comments):
        super().__init__(title, author, date, url, text, type)
        self.__num_comments = num_comments

    # accesseur
    def obtenir_num_comments(self):
        return self.__num_comments

    # mutateur
    def definir_num_comments(self, value):
        self.__num_comments = value
    
    def obtenirType(self):
        return self.type

    def __str__(self):
        return f"Document Reddit :\nTitre : {self.title}\nAuteur : {self.author}\nDate : {self.date}\nURL : {self.url}\nNombre de Commentaires : {self.__num_comments}\nType de Document : {self.type}"

class ArxivDocument(Document):
    def __init__(self, title, author, date, url, text, type, authors):
        super().__init__(title, author, date, url, text, type)
        self.__authors = authors

    # accesseur
    def obtenir_authors(self):
        return self.__authors

    # mutateur
    def definir_authors(self, value):
        self.__authors = value
    
    def obtenirType(self):
        return self.type
    
    def __str__(self):
        return f"Document Arxiv :\nTitre : {self.title}\nAuteur : {self.author}\nDate : {self.date}\nURL : {self.url}\nType de Document : {self.type}"


if __name__ == "__main__":
    # Exemple d'utilisation :
    doc = Document("Titre exemple", "Jean Dupont", "2023-12-31", "https://exemple.com", "Ceci est un texte d'exemple.", "Reddit")
    doc.afficher_infos()  # Pour afficher toutes les informations
    print()
    print(doc)            # Pour afficher la version digeste
