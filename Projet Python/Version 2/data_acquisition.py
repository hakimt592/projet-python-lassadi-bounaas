import praw
import urllib.request
import xmltodict
from datetime import datetime
import pandas as pd

docs = []
origins = []
# ============= REDDIT ==============
reddit = praw.Reddit(client_id='XiXfRk3WZnYOk1rfQL3kNQ', client_secret='Hk7H7Dz1OkXBYtGAZto-7Z212qkyhA', user_agent='Scrapping Website')
hot_posts = reddit.subreddit('MachineLearning').hot(limit=2)

for post in hot_posts:
    print("Titre")
    print(post.title)
    
    print("\nTexte")
    tmp = post.selftext.replace("\n", " ")
    print(tmp)

    print(f"\nPublié le : {datetime.utcfromtimestamp(post.created_utc)}")

    print(f"\nURL : {post.url}")

    print(f"\nAuteur : {post.author}")

    docs.append(tmp)
    origins.append("reddit")
    print("------------------------------------------")

# ============ ARXIV ===============
# Attributs de l'API ARXIV
    # id
    # updated
    # published
    # title
    # summary
    # author
    # arxiv:comment
    # link
    # arxiv:primary_category
    # category

try:
    with urllib.request.urlopen('http://export.arxiv.org/api/query?search_query=all:MachineLearning') as response:
        xml_response = response.read().decode('utf-8')
        parsed_response = xmltodict.parse(xml_response)
        
        entries = parsed_response['feed']['entry']
        
        for entry in entries:
            
            if 'title' in entry:
                print("Titre")
                print(entry['title'])
            print("\n")
            if 'summary' in entry:
                print("Résumé")
                tmp = entry['summary'].replace("\n", " ")
                print(tmp)
                
                docs.append(tmp)
                origins.append("arxiv")
            
            published = datetime.fromisoformat(entry['published'].replace('Z', '+00:00'))
            print(f"\nPublié le : {published}")

            if isinstance(entry['link'], list):
                link = entry['link'][0]["@href"]
                print(f"\nURL : {link}")

            print()
            for author in entry['author']:
                print(f"Auteur : {author['name']}")
            print("-------------------------------------------------")

            

except urllib.error.HTTPError as e:
    print(f"Erreur HTTP : {e.code} - {e.reason}")
except urllib.error.URLError as e:
    print(f"Erreur URL : {e.reason}")


# =================== PARTIE 2 : Sauvegarde et Chargement avec Pandas ===========================.

# Générer des identifiants uniques pour les textes
ids = list(range(1, len(docs) + 1))

# Créer un DataFrame
data = {'ID': ids, 'Texte': docs, 'Origine': origins}
df = pd.DataFrame(data)

# Sauvegarder le DataFrame dans un fichier CSV avec une tabulation comme séparateur
df.to_csv('donnees_texte.csv', sep='\t', index=False)


# Charger le fichier CSV dans un DataFrame
corpus = pd.read_csv('donnees_texte.csv', sep='\t')
# Afficher le DataFrame chargé
print(corpus)


# =================== PARTIE 3 : Sauvegarde et Chargement avec Pandas ===========================.

# Afficher la taille du corpus
print(f"\nTaille du corpus : {len(corpus['Texte'])}")

# Parcourir chaque ligne dans la colonne "Texte"
for index, row in corpus.iterrows():
    text = row['Texte']
    mots = text.split(" ")
    phrases = text.split(".")
    print(f"Texte à l'index {index}: Longueur des mots : {len(mots)}, Longueur des phrases : {len(phrases)}")

# Supprimer les documents ayant moins de 30 caractères.
corpus = corpus[corpus['Texte'].str.len() >= 30]
print(corpus)

# Créer une seule chaîne de caractères contenant tous les documents en utilisant la fonction join
texte_liste = df['Texte'].tolist()
# Joindre toutes les valeurs textuelles en une seule chaîne
texte_combine = ''.join(texte_liste)


print("\n" + texte_combine)
