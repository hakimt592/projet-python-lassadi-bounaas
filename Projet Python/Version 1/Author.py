class Author:
    def __init__(self, name):
        self.name = name
        self.ndoc = 0
        self.production = {}

    def ajouter_document(self, document):
        if document.author == self.name:
            self.production[self.ndoc] = document
            self.ndoc += 1

    def taille_moyenne_document(self):
        counter = 0
        for document in self.production.values():
            counter += len(document.text)
        try:
            moyenne = counter/len(self.production)
            return moyenne
        except ZeroDivisionError:
            return 0

    def __str__(self):
        return f"Nom de l'auteur : {self.name}, Nombre de documents publiés : {self.ndoc}, Taille moyenne des documents : {self.taille_moyenne_document()}"

if __name__ == "__main__":
    auteur = Author("Jean Dupont")

    # Exemple d'utilisation :
    from Document import Document
    doc1 = Document("Titre 1", auteur.name, "2023-01-01", "https://exemple.com/doc1", "Exemple de texte", "Reddit")
    doc2 = Document("Titre 2", auteur.name, "2023-02-01", "https://exemple.com/doc2", "Exemple de texte 2", "Arxiv")

    auteur.ajouter_document(doc1)
    auteur.ajouter_document(doc2)

    print(auteur)  # Pour afficher les informations de l'auteur en utilisant __str__
